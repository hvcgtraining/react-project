import React, { Component } from "react";
import "./Login.css";
import fire from "../config/Firebase";

import { Link } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.signup = this.signup.bind(this);
    this.state = {
      email: "",
      password: ""
    };
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  login(e) {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {})
      .catch(error => {
        console.log(error);
      });
  }

  signup(e) {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {})
      .then(u => {
        console.log(u);
      })
      .catch(error => {
        console.log(error);
      });
  }
  render() {
    return (
      <div className="Login">
        <form className="login-form">
          <h1>Login</h1>
          <div className="form-group">
            <input
              onChange={this.handleChange}
              value={this.state.email}
              type="text"
              placeholder="Username"
              name="email"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleChange}
              value={this.state.password}
              type="password"
              placeholder="Password"
              name="password"
            />
          </div>
          <input
            onClick={this.login}
            type="submit"
            className="login-btn"
            value="Login"
          />
          <div className="form-text">
            Don't have account? <Link to="/sign-up">Sign Up</Link>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;
