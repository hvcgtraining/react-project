import React, {Component} from 'react';
import {
    Row, 
    Col } from 'reactstrap';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import fire from '../config/Firebase'


import './Dashboard.css';

class Dashboard extends Component {
    logout = () => {
        fire.auth().signOut();
    }


    render(){

    
        return(
            <Router>
                <Row>
                    <Col sm="2" className="sidebar">
                        <div className="navbar navbar-cus">
                            <p className="title-logo">this is logo</p>
                        </div>
                        <ul className="sidebar-menu">
                            <li><Link to='/' className="sidebar-link"><span className="icon-holder"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCfUuRyvPGA5nA7SPMtJA96nwUuPMSx321r2RtyaQzV-Fo-wkt" alt="" /></span><span className="sidebar-tt">Home</span></Link></li>
                            <li><Link to='/Science' className="sidebar-link" href='/'><span className="icon-holder"><img src="https://cdn.iconscout.com/icon/premium/png-256-thumb/science-102-143128.png" alt="" /></span><span className="sidebar-tt">Science</span></Link></li>
                            <li><Link to='/Socienty'className="sidebar-link" href='/'><span className="icon-holder"><img src="https://www.nicepng.com/png/detail/239-2396381_join-us-society-icon-png.png" alt=""/></span><span className="sidebar-tt">Society</span></Link></li>
                            <li><Link to='/Sport'className="sidebar-link" href='/'><span className="icon-holder"><img src="https://cdn0.iconfinder.com/data/icons/stroke-ball-icons-2/633/02_Soccer-512.png" alt=""/></span><span className="sidebar-tt">Sport</span></Link></li>
                        
                        </ul>
                    </Col>
                    <Col sm="10" className="main">
                        <Row>
                            <Col sm="12" className="navbar navbar-cus">
                                <ul className="nav-right">
                                    <li>
                                        <a className="link-user" href='/'>
                                            <img className="img-user" src="https://icon-library.net/images/default-user-icon/default-user-icon-23.jpg" alt=""/>
                                            <span className="user-name-in">User</span>
                                        </a>
                                        <a onClick = {this.logout} className="link-logout" href="/">
                                            <img className="img-logout" src="https://library.kissclipart.com/20180831/ryq/kissclipart-logout-icon-png-red-clipart-computer-icons-abmeldu-ebde12cd7b0e3609.jpg" alt=""/>
                                        </a>
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                        <Row className="main-body">
                            <Route exact path="/" component={Home} />
                            <Route path="/Science" component={Science} />
                            <Route path="/Socienty" component={Socienty} />
                            <Route path="/Sport" component={Sport} />
                        </Row>
                    </Col>
                </Row>
            </Router>
        )
    }
}


export default Dashboard;

let Home = () => {
    return(
        <div>
            <p>hello</p>
        </div>
    )
}


let Science = () => {
    return(
        <div>
            <p>this is Science</p>
        </div>
    )
}

let Socienty = () => {
    return(
        <div>
            <p>this is Socienty</p>
        </div>
    )
}

let Sport = () => {
    return(
        <div>
            <p>this is Sport</p>
        </div>
    )
}
