import React, { Component } from "react";
import Login from "./Login";
import Signup from "./Signup";
import { BrowserRouter as Router, Route } from "react-router-dom";

class Home extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Login} />
          <Route path="/sign-up" component={Signup} />
        </div>
      </Router>
    );
  }
}

export default Home;
