import React, { Component } from "react";
import fire from "../config/Firebase";
import { Link } from "react-router-dom";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.signup = this.signup.bind(this);
    this.state = {
      email: "",
      password: ""
    };
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  signup = e => {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {})
      .then(u => {
        console.log(u);
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return (
      <div className="Login">
        <form className="login-form">
          <h1>Sign Up</h1>
          <div className="form-group">
            <input
              onChange={this.handleChange}
              value={this.state.email}
              type="text"
              placeholder="Username"
              name="email"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleChange}
              value={this.state.password}
              type="password"
              placeholder="Password"
              name="password"
            />
          </div>
          <input
            onClick={this.signup}
            type="submit"
            className="login-btn"
            value="Sign Up"
          />
          <div className="form-text">
            Already have an account? <Link to="/">Sign In</Link>
          </div>
        </form>
      </div>
    );
  }
}

export default Signup;
