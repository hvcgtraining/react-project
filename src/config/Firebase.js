import firebase from "firebase";

// Firebase Setup
const firebaseConfig = {
  apiKey: "AIzaSyB_pk91zkmcZcnR_H3lhDIBzU1dA8Z6TIU",
  authDomain: "dashboard-b15d9.firebaseapp.com",
  databaseURL: "https://dashboard-b15d9.firebaseio.com",
  projectId: "dashboard-b15d9",
  storageBucket: "",
  messagingSenderId: "136327343602",
  appId: "1:136327343602:web:a949e50c291624c8"
};
// Initialize Firebase
const fire = firebase.initializeApp(firebaseConfig);

export default fire;
